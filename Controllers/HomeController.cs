using Microsoft.AspNetCore.Mvc;
using Todo.Data;
using Todo.Models;

namespace Todo.Controllers{

    [ApiController]
    public class HomeController : ControllerBase{
        
        [HttpGet("/")]
        public IActionResult Get([FromServices] AppDbContext db) => Ok(db.TodoItems.ToList());
        

        [HttpGet("/{id}")]
        public IActionResult GetById(
            [FromRoute] int id,
            [FromServices] AppDbContext db)
        {
            TodoModel? model = db.TodoItems.Find(id);
            if(model == null) return NotFound();

            return Ok(model);
        }

        [HttpPost("/")]
        public IActionResult Post(
        [FromBody] TodoModel model, 
        [FromServices] AppDbContext db){
            db.TodoItems.Add(model);
            db.SaveChanges();
            return Created($"/{model.Id}", model);
        }

        [HttpPut("/{id}")]
        public IActionResult Put(
        [FromRoute] int id, 
        [FromBody] TodoModel todo,
        [FromServices] AppDbContext db){
            TodoModel? model = db.TodoItems.FirstOrDefault(c => c.Id == id);
            if (model == null) return NotFound();
            
            model.Title = todo.Title;
            model.Done = todo.Done;

            db.Update(model);
            db.SaveChanges();

            return Ok(model);
        }

        [HttpDelete("/{id}")]
        public IActionResult Delete([FromRoute] int id, [FromServices] AppDbContext db){
            var model = db.TodoItems.FirstOrDefault(c => c.Id == id);
            if (model == null) return NotFound();

            db.TodoItems.Remove(model);
            db.SaveChanges();

            return Ok(model);
        }
    }
}