using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Todo.Models;

namespace Todo.Data{
    public class AppDbContext : DbContext{

        public DbSet<TodoModel> TodoItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
          => options.UseSqlite("Data Source=app.db;Cache=shared");
    
    }
}